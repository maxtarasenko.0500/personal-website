import { expect as _expect, use } from 'chai';
// import { expect as sinon} from 'sinon';
import sinon from 'sinon';
// import { expect as sinonChai } from 'sinon-chai';
import sinonChai from 'sinon-chai';
import { validate, validateAsync, validateWithThrow, validateWithLog } from '../scripts/email-validator.mjs';

const expect = _expect;
use(sinonChai)

describe('Validate email - basic functionality', () => {
    it('Should return false on empty string', () => {
        const actual = validate('');
        expect(actual).to.equal(false);
    })
    it('Should return false on too short string', () => {
        const actual = validate('a');
        expect(actual).to.equal(false);
    })
    it('Should return false on incorrect string', () => {
        const actual = validate('email@hello.world');
        expect(actual).to.equal(false);
    })
    it('Should return true on correct gmail string', () => {
        const actual = validate('email@gmail.com');
        expect(actual).to.equal(true);
    })
    it('Should return true on correct outlook string', () => {
        const actual = validate('email@outlook.com');
        expect(actual).to.equal(true);
    })
    it('Should return true on correct yandex string', () => {
        const actual = validate('email@yandex.ru');
        expect(actual).to.equal(true);
    })
})

describe('Async email validation - basic functionality', () => {
    it('Should return false on empty string', async () => {
        let result
        await validateAsync('').then(() => {
            result = true
        }).catch(() => {
            result = false;
        })
        expect(result).to.equal(false)
    })
    it('Should return false on too short string', async () => {
        let result
        await validateAsync('a').then(() => {
            result = true
        }).catch(() => {
            result = false;
        })
        expect(result).to.equal(false)
    })
    it('Should return false on incorrect string', async () => {
        let result
        await validateAsync('email@hello.world').then(() => {
            result = true
        }).catch(() => {
            result = false;
        })
        expect(result).to.equal(false)
    })
    it('Should return true on correct gmail string', async () => {
        let result
        await validateAsync('email@gmail.com').then(() => {
            result = true
        }).catch(() => {
            result = false;
        })
        expect(result).to.equal(true)
    })
    it('Should return true on correct outlook string', async () => {
        let result
        await validateAsync('email@outlook.com').then(() => {
            result = true
        }).catch(() => {
            result = false;
        })
        expect(result).to.equal(true)
    })
    it('Should return true on correct yandex string', async () => {
        let result
        await validateAsync('email@yandex.ru').then(() => {
            result = true
        }).catch(() => {
            result = false;
        })
        expect(result).to.equal(true)
    })
})

describe('Throw error on invalid email - basic functionality', () => {
    it('Should throw error on empty string', () => {
        try {
            const actual = validateWithThrow('');
        } catch (e) {
            expect(actual).to.throw(TypeError, 'Message that the provided email is invalid');
        }
    })
    it('Should throw error on too short string', () => {
        try {
            const actual = validateWithThrow('a');
        } catch (e) {
            expect(actual).to.throw(TypeError, 'Message that the provided email is invalid');
        }
    })
    it('Should throw error on incorrect string', () => {
        try {
            const actual = validateWithThrow('email@hello.world');
        } catch (e) {
            expect(actual).to.throw(TypeError, 'Message that the provided email is invalid');
        }
    })
    it('Should return true on correct gmail string', () => {
        const actual = validateWithThrow('email@gmail.com');
        expect(actual).to.equal(true);
    })
    it('Should return true on correct outlook string', () => {
        const actual = validateWithThrow('email@outlook.com');
        expect(actual).to.equal(true);
    })
    it('Should return true on correct yandex string', () => {
        const actual = validateWithThrow('email@yandex.ru');
        expect(actual).to.equal(true);
    })
})


describe('Validate email with logs in console - basic functionality', () => {
    beforeEach(() => {
        sinon.spy(console, 'log');
    });

    afterEach(() => {
        console.log.restore();
    })

    it('Should return false in console on empty string', () => {
        validateWithLog('');
        expect(console.log).to.have.been.calledWith(false);
    })
    it('Should return false on too short string', () => {
        validateWithLog('a');
        expect(console.log).to.have.been.calledWith(false);
    })
    it('Should return false in console on incorrect string', () => {
        validateWithLog('email@hello.world');
        expect(console.log).to.have.been.calledWith(false);
    })
    it('Should return true in console on correct gmail string', () => {
        validateWithLog('email@gmail.com');
        expect(console.log).to.have.been.calledWith(true);
    })
    it('Should return true in console on correct outlook string', () => {
        validateWithLog('email@outlook.com');
        expect(console.log).to.have.been.calledWith(true);
    })
    it('Should return true in console on correct yandex string', () => {
        validateWithLog('email@yandex.ru');
        expect(console.log).to.have.been.calledWith(true);
    })
})
