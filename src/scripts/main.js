import { SectionCreator, EventManager } from './join-us-section.js';
import WebsiteSection from './section-creator.js';
import '../styles/style.css';
import CreateRequest from './fetch-request.js';
import { returnData } from './performance.js';

customElements.get('website-section') || customElements.define('website-section', WebsiteSection)

document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
        const factory = new SectionCreator();
        const type = factory.selectType('standart');
        const requestManager = new CreateRequest();

        returnData().then((values) => {
            const metrics = {
                'pageLoad': values[0],
                'bcLoad': values[1]
            };
            requestManager.sendRequest('api/analytics/performance', metrics)
        
        })


        factory.displaySectionJOP(type);
        factory.displaySectionBC();

        const manager = new EventManager();

        manager.saveEmail(type);


        if (window.Worker) {
            const worker = new Worker('./scripts/worker.js')
            const allButtonsToStore = document.getElementsByClassName("app-section__button");

            for (let x = 0; x < allButtonsToStore.length; x++) {
                allButtonsToStore[x].addEventListener("click", function () {
                    worker.postMessage(this.textContent)
                });
            }

            worker.onmessage = (e) => {
                requestManager.sendRequest('api/analytics/user', e.data)
            }

            worker.onerror = (error) => {
                console.log(error.message)
            }
        }
    }
};