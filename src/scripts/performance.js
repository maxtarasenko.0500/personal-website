const perf = (type, name, time, size = '') => {
    return `${type}: ${name} | ${time ? Math.round(time) + 'ms' : ''} ${size}`
}



function measureBCInformationGetting(callback) {
    window.addEventListener('load', () => {
        const userObserver = new PerformanceObserver((list) => {
            list.getEntries().forEach((entry) => {
                callback(perf(entry.entryType, entry.name, entry.entryType == "mark" ? entry.startTime : entry.duration))
            })
        })
        userObserver.observe({ entryTypes: ['mark', 'measure'] })
    })
}

function measurePageLoadMemoryUsage(callback) {
    window.addEventListener('load', () => {
        const loadMemory = performance.getEntriesByType('resource');
        const loadTime = performance.getEntriesByType('navigation')[0];
        let memory = 0
        loadMemory.forEach((entry) => {
            memory += Math.round(entry.encodedBodySize / 1024);
        })
        memory = `${memory}Kb`
        callback(perf('measure', 'PageLoad', loadTime.fetchStart, memory))
    })
}
const returnData = () => {
    return Promise.all([
        new Promise((resolve) => {
            window.addEventListener('load', () => {
                const loadMemory = performance.getEntriesByType('resource');
                const loadTime = performance.getEntriesByType('navigation')[0];
                let memory = 0
                loadMemory.forEach((entry) => {
                    memory += Math.round(entry.encodedBodySize / 1024);
                })
                memory = `${memory}Kb`
                resolve(perf('measure', 'PageLoad', loadTime.fetchStart, memory))
            })
        }),
        new Promise((resolve) => {
            window.addEventListener('load', () => {
                const userObserver = new PerformanceObserver((list) => {
                    list.getEntries().forEach((entry) => {
                        resolve(perf(entry.entryType, entry.name, entry.entryType == "mark" ? entry.startTime : entry.duration))
                    })
                })
                userObserver.observe({ entryTypes: ['mark', 'measure'] })
            })
        })
    ])
}


export { returnData }
