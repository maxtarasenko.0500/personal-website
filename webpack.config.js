const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: './src/scripts/main.js',

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                ],
            },

        ],
    },

    plugins: [
        new HtmlWebpackPlugin({ template: 'src/index.html' }),
        new CopyPlugin({
            patterns: [
                {
                    from: './src/assets/',
                    to: path.resolve(__dirname, './bundle'),
                },
            ],
        }),
    ],

    devServer: {
        static: {
            directory: path.join(__dirname, './src'),
        },
        port: 9000,
        hot: true,
        proxy: {
            '/api': {
                target: 'http://localhost:3000',
                pathRewrite: { '^/api': '' },

            },
        },
    },

    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()],
    },

    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, './bundle'),
    },
}