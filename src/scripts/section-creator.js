class WebsiteSection extends HTMLElement {
    constructor() {
        super();
    }

    static get observedAttributes() {
        return ['title', 'main-title', 'description', 'image-link', 'picture-class']
    }

    addBackground() {
        const href = this.getAttribute('image-link')
        const pictureClass = this.getAttribute('picture-class')
        this.innerHTML += `<img src="${href}" alt="Logo icon picture" class="${pictureClass}" />`
    }

    addMainTitleText() {
        const title = this.getAttribute('main-title')
        const description = this.getAttribute('description')
        this.innerHTML += `
            <h1 class="app-title">
                ${title}
            </h1>
            <h2 class="app-subtitle">
                ${description}
            </h2>
        `
    }

    addTitleText() {
        const title = this.getAttribute('title')
        const description = this.getAttribute('description')
        this.innerHTML += `
            <h2 class="app-title app-title--ordinary">
                ${title}
            </h2>
            <h3 class="app-subtitle app-description--ordinary">
                ${description}
            </h3>
            <style>
                .app-title--ordinary {
                    order: -2;
                }
                .app-description--ordinary {
                    order: -1;
                }
            </style>
        `
    }

    connectedCallback() {
        // Image
        if (this.getAttribute('image-link')) {
            this.addBackground();
        }
        // Main Title
        if (this.getAttribute('main-title')) {
            this.addMainTitleText();
        }
        // Title
        if (this.getAttribute('title')) {
            this.addTitleText();
        }

    }

}

export default WebsiteSection;