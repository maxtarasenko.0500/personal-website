import CreateRequest from './fetch-request.js';
import { validate } from './email-validator.mjs';

const requestManager = new CreateRequest();

class SectionCreator {
    displaySectionJOP(type) {
        const jopSection = document.createElement('section');

        const element = document.querySelector('.app-footer');
        element.insertAdjacentElement('beforebegin', jopSection).classList.add('app-section', 'app-section--join-program');

        const text = document.createElement('div');
        text.classList.add('app-text', 'app-text--join-program');
        jopSection.appendChild(text);

        const title = document.createElement('h2')
        title.classList.add('app-title', 'app-title--join-program');
        title.innerText = type.title;
        text.appendChild(title);

        const subtitle = document.createElement('h3')
        subtitle.classList.add('app-subtitle', 'app-subtitle--join-program');
        subtitle.innerText = 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        text.appendChild(subtitle);

        const inputMenu = document.createElement('form')
        inputMenu.classList.add('app-input-menu');
        jopSection.appendChild(inputMenu);

        const input = document.createElement('input');
        input.classList.add('app-input');
        input.type = 'text';
        input.placeholder = 'Email'
        inputMenu.appendChild(input);

        const button = document.createElement('input');
        button.classList.add('app-section__button', 'app-section__button--join-program');
        button.type = 'submit';
        button.value = type.submitButton;
        inputMenu.appendChild(button);
    }

    displaySectionBC() {
        const sectionUrl = 'api/community';
        performance.mark('SectionBcStart');
        requestManager.getRequest(sectionUrl).then((data) => {
            performance.mark('SectionBcEnd');
            performance.measure('SectionBC', 'SectionBcStart', 'SectionBcEnd')
            if (data) {
                const bcSection = document.createElement('website-section');

                const element = document.querySelector('.app-section--image-culture');
                element.insertAdjacentElement('beforebegin', bcSection).classList.add('app-section', 'app-section--big-community');


                const text = document.createElement('div');
                text.classList.add('app-text', 'app-text--big-community');
                bcSection.appendChild(text);

                const title = document.createElement('h2')
                title.classList.add('app-title', 'app-title--big-community');
                title.innerText = 'Big Community of People Like You ';
                text.appendChild(title);

                const subtitle = document.createElement('h3')
                subtitle.classList.add('app-subtitle', 'app-subtitle--big-community');
                subtitle.innerText = 'We’re proud of our products, and we’re really excited when we get feedback from our users.';
                text.appendChild(subtitle);


                const information = document.createElement('div')
                information.classList.add('app-people-information')
                bcSection.appendChild(information);

                const formIformation = data;

                formIformation.forEach((person) => {

                    const form = document.createElement('div')
                    form.classList.add('app-people')
                    information.append(form)

                    const avatar = document.createElement('img')
                    avatar.classList.add('app-people--avatar')
                    avatar.src = person.avatar
                    avatar.alt = 'Avatar'
                    form.append(avatar)

                    const description = document.createElement('p')
                    description.classList.add('app-people--description')
                    description.innerText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.'
                    form.append(description)

                    const name = document.createElement('div')
                    name.classList.add('app-people--names')
                    name.innerText = (`${person.firstName} ${person.lastName}`)
                    form.append(name)

                    const post = document.createElement('div')
                    post.classList.add('app-people--post')
                    post.innerText = person.position
                    form.append(post)
                })
            }

        })
    }

    selectType(type) {
        switch (type) {
            case 'advanced':
                return {
                    title: 'Join Our Advanced Program',
                    submitButton: 'Subscribe to Advanced Program',
                }
            case 'standart':
                return {
                    title: 'Join Our Program',
                    submitButton: 'submit',
                }
        }
    }

    remove() {
        // eslint-disable-next-line no-unused-expressions
        document.querySelector('.app-section--join-program').remove
    }
}

class EventManager {
    constructor() {
        this.input = document.querySelector('.app-input')
        this.button = document.querySelector('.app-section__button--join-program')
        this.serverUrlSubscribe = 'api/subscribe';
        this.serverUrlUnsubscribe = 'api/unsubscribe';

    }

    saveInput() {
        this.input.addEventListener('input', (e) => {
            e.preventDefault()
            // eslint-disable-next-line no-alert
            localStorage.setItem('isUserSubscribed', true);
        }, false)
    }

    saveEmail(type) {
        this.button.addEventListener('click', (e) => {
            e.preventDefault()
            if (this.button.value !== 'unsubscribe') {
                if (validate(this.input.value)) {
                    this.input.setAttribute('disabled', 'disabled')
                    this.button.setAttribute('disabled', 'disabled')
                    requestManager.sendRequest(this.serverUrlSubscribe, this.input.value, (result) => {
                        if (result) {
                            localStorage.setItem('mainEmail', this.input.value)
                            this.input.style.display = 'none';
                            this.button.value = 'unsubscribe';
                        }
                        this.input.removeAttribute('disabled')
                        this.button.removeAttribute('disabled')
                    });
                }
            } else {
                this.input.style.display = 'block';
                requestManager.sendRequest(this.serverUrlUnsubscribe, this.input.value, (result) => {
                    this.input.value = '';
                    this.button.value = type.submitButton;
                    localStorage.clear()
                });
            }
        }, false);
    }

    checkLocalStorage() {
        if (localStorage.getItem('mainEmail') !== null) {
            this.input.style.display = 'none';
            this.button.value = 'unsubscribe'
        }
    }

}

export { SectionCreator, EventManager };
