function handleErrors(response) {
    if (response.status === 422) {
        // eslint-disable-next-line no-alert
        response.json().then((data) => {
            // eslint-disable-next-line no-alert
            window.alert(data.error)
            callback(false)
        })
        return false;
    }
}

function handleSuccess(response, callback) {
    if (response.status === 200) {
        // eslint-disable-next-line no-alert
        if (callback) {
            callback(true);
        }
    }

}

class CreateRequest {
    sendRequest(url, data, callback) {
        fetch(url, {
            method: 'post',
            headers: {
                'Content-type': 'application/json',
                'Content-Security-Policy': 'script-src "*"'
            },
            body: JSON.stringify({ email: data }),
        })
            .then((response) => {
                handleErrors(response);
                handleSuccess(response, callback)
            })
    }


    async getRequest(url, callback) {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
}

export default CreateRequest;