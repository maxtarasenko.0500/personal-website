module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [],
  overrides: [
    {
      files: '*.js',
      rules: {
        'linebreak-style': 'off',
        'padded-blocks': 'off',
        'no-undef': 'off',
        'class-methods-use-this': 'off',
        'no-console': 'off',
        'no-multiple-empty-lines': 'off',
        semi: 'off',
        'object-curly-spacing': 'off',
        // eslint-disable-next-line no-dupe-keys
        'class-methods-use-this': 'off',
        'default-case': 'off',
        'keyword-spacing': 'off',
        'consistent-return': 'off',
        indent: 'off',
        'no-const-assign': 'off',
        'no-plusplus': 'off',
        'eol-last': 'off',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
  },
};
