const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];


function validate(email) {
    let result = false;
    for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
        let item = VALID_EMAIL_ENDINGS[i];
        if (email.length >= item.length && email.endsWith(item)) {
            result = true;
        }
    }
    return result;
}

async function validateAsync(email) {
    return new Promise((resolve, reject) => {
        for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
            let item = VALID_EMAIL_ENDINGS[i];
            if (email.length >= item.length && email.endsWith(item)) {
                resolve(true);
                break;
            }
        }
        reject(false);
    })
}

function validateWithThrow(email) {
    for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
        let item = VALID_EMAIL_ENDINGS[i];
        if (email.length >= item.length && email.endsWith(item)) {
            return true;
        }
    }
    return new TypeError('Message that the provided email is invalid');
}

function validateWithLog(email) {
    let result = false;
    for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
        let item = VALID_EMAIL_ENDINGS[i];
        if (email.length >= item.length && email.endsWith(item)) {
            result = true;
        }
    }
    console.log(result)
    return result;
}

export { validate, validateAsync, validateWithThrow, validateWithLog };